const { prisma } = require('./generated/prisma-client');
const { GraphQLServer } = require('graphql-yoga');

const resolvers = {
  Query: {
    dealers(root, args, context) {
      return context.prisma.dealers();
    },
  },
  Mutation: {
    createDealer(root, args, context) {
      console.log('args', args);

      const {
        local_office_name,
        division,
        district,
        upazila,
        union,
        institution_name,
        owner_name,
        owner_phone,
        owner_NID_num,
        distance_storage_to_sellshouse,
        date_of_application,
        date_of_security_deposit,
        date_of_deal,
        last_date_of_dealership_renewal,
        last_date_of_supply_order,
        last_index_of_supply_order,
        last_payment_date_of_supply_order,
        supply_order_index_of_last_payment,
        product_upload_dates,
      } = args;

      // validation

      return context.prisma.createDealer({
        data: {
          local_office_name,
          division,
          district,
          upazila,
          union,
          institution_name,
          owner_name,
          owner_phone,
          owner_NID_num,
          distance_storage_to_sellshouse,
          date_of_application,
          date_of_security_deposit,
          date_of_deal,
          last_date_of_dealership_renewal,
          last_date_of_supply_order,
          last_index_of_supply_order,
          last_payment_date_of_supply_order,
          supply_order_index_of_last_payment,
          product_upload_dates,
        },
      });
    },
  },
};

const server = new GraphQLServer({
  typeDefs: './schema.graphql',
  resolvers,
  context: {
    prisma,
  },
});
server.start(() => console.log('Server is running on http://localhost:4000'));
